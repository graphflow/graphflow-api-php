<?php
/**
 *  Copyright 2011 Wordnik, Inc.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 *
 * NOTE: This class is auto generated by the swagger code generator program. Do not edit the class manually.
 */
class UserApi {

	function __construct($apiClient) {
	  $this->apiClient = $apiClient;
	}

  /**
	 * putUserById
	 * Updates a User by ID
   * body, User: JSON User userData - NOTE 'userId' is required. (required)
   * @return User
	 */

   public function putUserById($body) {

  		//parse inputs
  		$resourcePath = "/user/";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "PUT";
      $queryParams = array();
      $headerParams = array();

      //make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'User');
  		return $responseObject;

      }
  /**
	 * postUser
	 * Add a new User
   * body, User: 
JSON User consisting of fields 'userId' and 'userData'.
'userId' must be a Long and unique amongst all Users.
'userData' is a Map of (String, String) key-value pairs.
     (required)
   * @return User
	 */

   public function postUser($body) {

  		//parse inputs
  		$resourcePath = "/user/";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "POST";
      $queryParams = array();
      $headerParams = array();

      //make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'User');
  		return $responseObject;

      }
  /**
	 * getInteractions
	 * Gets all the interactions between a User and an Item or another User, of type 'type'
   * userId, string: The ID of the User (required)
   * type, string: The TYPE of the Interaction (Default = ALL). Can be multi-valued, hence '?userId=1&amp;type=view&amp;type=purchase' will retrieve both 'view' and 'purchase' interactions (optional)
   * num, int: Number of interactions to return. Most recent are returned first (Default = 20) (optional)
   * withDetails, bool: Whether or not to include data of from and to objects in the results (Default = false) (optional)
   * @return array[Interaction]
	 */

   public function getInteractions($userId, $type=null, $num=null, $withDetails=null) {

  		//parse inputs
  		$resourcePath = "/user/interaction";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "GET";
      $queryParams = array();
      $headerParams = array();

      if($userId != null) {
  		  $queryParams['userId'] = $this->apiClient->toQueryValue($userId);
  		}
  		if($type != null) {
  		  $queryParams['type'] = $this->apiClient->toQueryValue($type);
  		}
  		if($num != null) {
  		  $queryParams['num'] = $this->apiClient->toQueryValue($num);
  		}
  		if($withDetails != null) {
  		  $queryParams['withDetails'] = $this->apiClient->toQueryValue($withDetails);
  		}
  		//make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'array[Interaction]');
  		return $responseObject;

      }
  /**
	 * postInteraction
	 * Add an interaction between a User and an Item (or another User - NOT CURRENTLY SUPPORTED)
   * body, PostedInteraction: 
JSON interaction to post, of the form:
{&quot;fromId&quot;:&quot;id1&quot;,&quot;toId&quot;:&quot;id2&quot;,&quot;interactionType&quot;:&quot;view&quot;}.
An additional field 'timestamp' is optional.
If the interaction type is a 'purchase' or 'addtobasket', then valid entries for the fields
'quantity' and 'price' are required, e.g.
{&quot;fromId&quot;:&quot;id1&quot;,&quot;toId&quot;:&quot;id2&quot;,&quot;interactionType&quot;:&quot;purchase&quot;,&quot;quantity&quot;:1,&quot;price&quot;:45.99}
       (required)
   * @return Interaction
	 */

   public function postInteraction($body) {

  		//parse inputs
  		$resourcePath = "/user/interaction";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "POST";
      $queryParams = array();
      $headerParams = array();

      //make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'Interaction');
  		return $responseObject;

      }
  /**
	 * postInteractionList
	 * Add multiple interactions between a User and Item(s) (or another User - NOT CURRENTLY SUPPORTED)
   * body, array: 
JSON list of interactions to post. Each interaction is in the form:
{&quot;fromId&quot;:&quot;id1&quot;,&quot;toId&quot;:&quot;id2&quot;,&quot;interactionType&quot;:&quot;view&quot;}.
An additional field 'timestamp' is optional.
If the interaction type is a 'purchase' or 'addtobasket', then valid entries for the fields
'quantity' and 'price' are required, e.g.
{&quot;fromId&quot;:&quot;id1&quot;,&quot;toId&quot;:&quot;id2&quot;,&quot;interactionType&quot;:&quot;purchase&quot;,&quot;quantity&quot;:1,&quot;price&quot;:45.99}
       (required)
   * @return array[Interaction]
	 */

   public function postInteractionList($body) {

  		//parse inputs
  		$resourcePath = "/user/interactionlist";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "POST";
      $queryParams = array();
      $headerParams = array();

      //make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'array[Interaction]');
  		return $responseObject;

      }
  /**
	 * getUserById
	 * Get a User by ID
   * userId, string: Id of User to retrieve (required)
   * @return User
	 */

   public function getUserById($userId) {

  		//parse inputs
  		$resourcePath = "/user/{userId}";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "GET";
      $queryParams = array();
      $headerParams = array();

      if($userId != null) {
  			$resourcePath = str_replace("{" . "userId" . "}",
  			                            $this->apiClient->toPathValue($userId), $resourcePath);
  		}
  		//make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'User');
  		return $responseObject;

      }
  /**
	 * deleteUserById
	 * Delete a User by ID
   * userId, string: Id of User to delete (required)
   * @return User
	 */

   public function deleteUserById($userId) {

  		//parse inputs
  		$resourcePath = "/user/{userId}";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "DELETE";
      $queryParams = array();
      $headerParams = array();

      if($userId != null) {
  			$resourcePath = str_replace("{" . "userId" . "}",
  			                            $this->apiClient->toPathValue($userId), $resourcePath);
  		}
  		//make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'User');
  		return $responseObject;

      }
  /**
	 * getSimilarUsers
	 * Get Users that are similar to the provided User
   * userId, string: Id of User for which to retrieve similar Users (required)
   * userDetails, bool: Whether to return User IDs or the entire User in the results (Default = false) (optional)
   * purchasedTogether, bool: Whether to restrict the similar Users computation based on purchased and addtobasket behaviour (Default = false) (optional)
   * num, int: The number of similar items to retrieve to return (Default = 20) (optional)
   * @return array[UserRecommendation]
	 */

   public function getSimilarUsers($userId, $userDetails=null, $purchasedTogether=null, $num=null) {

  		//parse inputs
  		$resourcePath = "/user/{userId}/similar";
  		$resourcePath = str_replace("{format}", "json", $resourcePath);
  		$method = "GET";
      $queryParams = array();
      $headerParams = array();

      if($num != null) {
  		  $queryParams['num'] = $this->apiClient->toQueryValue($num);
  		}
  		if($userDetails != null) {
  		  $queryParams['userDetails'] = $this->apiClient->toQueryValue($userDetails);
  		}
  		if($purchasedTogether != null) {
  		  $queryParams['purchasedTogether'] = $this->apiClient->toQueryValue($purchasedTogether);
  		}
  		if($userId != null) {
  			$resourcePath = str_replace("{" . "userId" . "}",
  			                            $this->apiClient->toPathValue($userId), $resourcePath);
  		}
  		//make the API Call
      if (! isset($body)) {
        $body = null;
      }
  		$response = $this->apiClient->callAPI($resourcePath, $method,
  		                                      $queryParams, $body,
  		                                      $headerParams);


      if(! $response){
          return null;
        }

  		$responseObject = $this->apiClient->deserialize($response,
  		                                                'array[UserRecommendation]');
  		return $responseObject;

      }
  
}

